package com.example.petproject

import android.app.Application
import com.example.testsdk.TestSdk

class PetApp : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this

        TestSdk.init(this)
    }

    companion object {

        lateinit var instance: PetApp
            private set

        fun getInstance(): Application = instance
    }
}