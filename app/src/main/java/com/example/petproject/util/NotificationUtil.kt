package com.example.petproject.util

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.petproject.R

object NotificationUtil {

    private const val ID_NOTIFICATION = 12345678
    private const val CHANNEL_ID = "com.example.petproject.channel_id"
    private const val CHANNEL_NAME = "PET_PROJECT_CHANNEL"

    fun showNotification(context: Context, loginInfo: String) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            val notificationChannel =
                NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_DEFAULT)
            val notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(notificationChannel)
        }

        val builder = NotificationCompat.Builder(context, CHANNEL_ID)

        builder.setSmallIcon(android.R.drawable.ic_dialog_alert)
            .setContentTitle(context.getString(R.string.app_name))
            .setContentText(context.getString(R.string.format_notification_user, loginInfo))
            .setOngoing(true)
            .setAutoCancel(false)

        with(NotificationManagerCompat.from(context)) {
            val notification = builder.build()
            notification.flags = notification.flags or Notification.FLAG_NO_CLEAR
            notify(CHANNEL_ID, ID_NOTIFICATION, notification)
        }
    }

    fun removeNotification(context: Context) {
        NotificationManagerCompat.from(context).cancel(CHANNEL_ID, ID_NOTIFICATION)
    }
}